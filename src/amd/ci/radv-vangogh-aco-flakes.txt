dEQP-VK.dynamic_rendering.basic.*
dEQP-VK.dynamic_rendering.primary_cmd_buff.basic.*
dEQP-VK.draw.dynamic_rendering.*

dEQP-VK.pipeline.monolithic.image.suballocation.sampling_type.separate.view_type.2d_array.format.r4g4b4a4_unorm_pack16.count_4.size.32x16_array_of_6

# New flakes in CTS 1.3.7.0
dEQP-VK.dynamic_rendering.primary_cmd_buff.basic.partial_binding_depth_stencil
